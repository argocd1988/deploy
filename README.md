# Deploy

```bash
kubectl create namespace argocd
kubectl apply -f application.yaml

kubectl port-forward service/application-nginx 8081:80 -n argocd1988-ns

```
